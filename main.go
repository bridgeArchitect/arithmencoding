package main

import "fmt"

/* initial row */
const row = "ABABCDACDD"
/* initial number */
const number = 0.61344

/* structure to save range of real numbers */
type Range struct {
	points [2]float64
}

/* entry point */
func main() {

	/* declarations of variables */
	var (
		ranges         [4]Range
		end, begin     float64
		savBegin       float64
		dist           float64
		bias1, bias2   float64
		endA, beginA   float64
		endB, beginB   float64
		endC, beginC   float64
		endD, beginD   float64
		i              int
	)

	/* set ranges of every symbol */
	ranges[0].points[0] = 0.0
	ranges[0].points[1] = 0.3
	ranges[1].points[0] = 0.3
	ranges[1].points[1] = 0.5
	ranges[2].points[0] = 0.5
	ranges[2].points[1] = 0.7
	ranges[3].points[0] = 0.7
	ranges[3].points[1] = 1.0

	/* set begin and end of range */
	end = 1.0
	begin = 0.0

	/* go through all symbols */
	for i = 0; i < len(row); i++ {
		/* calculate distance */
		dist = end - begin
		/* find out biases */
		if row[i] == 'A' {
			bias1 = ranges[0].points[0] * dist
			bias2 = ranges[0].points[1] * dist
		} else if row[i] == 'B' {
			bias1 = ranges[1].points[0] * dist
			bias2 = ranges[1].points[1] * dist
		} else if row[i] == 'C' {
			bias1 = ranges[2].points[0] * dist
			bias2 = ranges[2].points[1] * dist
		} else if row[i] == 'D' {
			bias1 = ranges[3].points[0] * dist
			bias2 = ranges[3].points[1] * dist
		}
		/* calculate new ends of range */
		savBegin = begin
		begin = savBegin + bias1
		end = savBegin + bias2
	}

	/* print final result */
	fmt.Println(begin)
	fmt.Println(end)

	/* again to start process of search (search row using number) */
	end = 1.0
	begin = 0.0

	/* go though all symbols */
	for i = 0; i < 5; i++ {
		/* calculate distance */
		dist = end - begin
		/* find out all biases */
		beginA = begin + ranges[0].points[0] * dist
		endA = begin + ranges[0].points[1] * dist
		beginB = begin + ranges[1].points[0] * dist
		endB = begin + ranges[1].points[1] * dist
		beginC = begin + ranges[2].points[0] * dist
		endC = begin + ranges[2].points[1] * dist
		beginD = begin + ranges[3].points[0] * dist
		endD = begin + ranges[3].points[1] * dist
		/* find out new symbol and ends */
		if (number > beginA) && (number < endA) {
			begin = beginA
			end = endA
			fmt.Print("A")
		}
		if (number > beginB) && (number < endB) {
			begin = beginB
			end = endB
			fmt.Print("B")
		}
		if (number > beginC) && (number < endC) {
			begin = beginC
			end = endC
			fmt.Print("C")
		}
		if (number > beginD) && (number < endD) {
			begin = beginD
			end = endD
			fmt.Print("D")
		}

	}
	fmt.Println()

}